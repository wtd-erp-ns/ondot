/**
 * Module Description:
 *
 * Version       Date              Author                Remarks
 * 1.0           08 Feb 2021       Libor Nekula        Initial Version
 * 1.1           9 Feb 2021        Lucia Zacharova     Updated version
 *
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 */

define([
    "require",
    "exports",
    "./../lib/params",
    'N/task'
], function (require, exports) {
    const params = require("../lib/params");
    const task = require("N/task");
    var PARAMS = [
        {
            id: 'custscript_wdff_mr_script',
            isMandatory: true
        }
    ]

    const LOG_TITLE = 'WD_WDFF_Ondot.';

    /**
     * The recordType (internal id) corresponds to the "Applied To" record in your
     * script deployment.
     *
     * @appliedtorecord Item
     *
     * @param {Object} script context

     * @returns
     */
    exports.beforeSubmit = (scriptContext) => {

        const logTitle = LOG_TITLE + 'beforeSubmit';
        log.debug(logTitle);
        if(scriptContext.UserEventType.CREATE == scriptContext.type){
            return;
        }
        let wave = scriptContext.newRecord;
        let oldWave = scriptContext.oldRecord;
        let oldStatus = oldWave.getValue("status");
        let newStatus = wave.getValue("status");
        let exportStatuses = {"success": 1 , "failed": 2} //todo: put into library
        let exportStatus = exportStatuses.success;
        log.debug(logTitle,"Statuses - old:  " + oldStatus +" , new: "+newStatus);

        if(oldStatus != "RELEASED" || newStatus != "INPROGRESS"){
            log.debug(logTitle ,wave.id + ' - OnDot integration is not triggered, the wave statuses are not eligible for integration!')
            return;
        }

        // get parameters
        let paramsMap = params.getParameters(PARAMS);
        log.debug(logTitle, JSON.stringify(paramsMap));

        let mrScriptId = paramsMap.custscript_wdff_mr_script;
        let mrParams = {
            custscript_wdff_wave_id: wave.id
        }

        let objTask = task.create({
            taskType: task.TaskType.MAP_REDUCE,
            scriptId: mrScriptId,
            params: mrParams
        });

        log.debug(logTitle, 'mrParams: ' + JSON.stringify(mrParams));

        try {

            let taskId = objTask.submit();
            log.audit(logTitle, 'Process Task submitted' + taskId);

        } catch (e) {

            log.error("ERROR", JSON.stringify(e))
            exportStatus = exportStatuses.failed;

        } finally {

            wave.setValue({fieldId:"custbody_wdff_ondot_exp_statuses", value:exportStatus})
            // Here I would put logic that submits success/error message to custom field on wave
        }
    }
});
