/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 */
define(["require", "exports", 'N/record',"../../3pl-integration/lib/params","../../3pl-integration/lib/config","N/search","N/runtime","../lib/wd_ondot_functions"],
    /**
 * @param{https} https
 * @param{record} record
 */
    (require, exports) => {

        const record = require("N/record");
        const params = require("../../3pl-integration/lib/params");
        const config = require("../../3pl-integration/lib/config");
        const runtime = require("N/runtime");
        const ondotLib = require("../lib/wd_ondot_functions");

        const LOG_TITLE = 'WD_WDFF_Ondot_Send_Cartons - ';
        const PARAMS = [
            {
                id: 'custscript_wdff_ondot_configuration',
                isMandatory: true
            },
            {
                id: 'custscript_wdff_ue_override_setting',
                isMandatory: true
            },
            {
                id: 'custscript_wdff_ue_use_prod_urls',
                isMandatory: false
            },
            {
                id: 'custscript_wd_wave_trigger',
                isMandatory: false
            },
            {
                id: 'custscript_wd_applicable_locations',
                isMandatory: true
            }
        ]
        const prodAccount = true;

        const ONDOT_ERROR_CARRIER = 1999;
        const SUCCESS_STATUS_ID = 1;
        const PARTIAL_SUCCESS_ID = 3;
        const SHIPMENT_EXPORTED = 4;
        const HERMES_SHIPMETHOD_IDS = prodAccount ? ["1379","2921"] : ["1379","2714"];
        const DHL_SHIPMETHOD_IDS = prodAccount ? ["1373","2961","2922","2917"] : ["1373","2711"];
        const ERROR_STATUS_ID = 2;
        const PICKED_STATUS = "A";
        const PACKED_STATUS = "B";




        /**
         * Defines the function definition that is executed after record is submitted.
         * @param {Object} scriptContext
         * @param {Record} scriptContext.newRecord - New record
         * @param {Record} scriptContext.oldRecord - Old record
         * @param {string} scriptContext.type - Trigger type; use values from the context.UserEventType enum
         * @since 2015.2
         */
        const afterSubmit = (context) => { //aftersubmit used instead of before submit because we have problems with inv detials
            let itemFulfillmentRec = context.newRecord;
            let itemFulfillmentId = itemFulfillmentRec.getValue('id');
            let logTitle = LOG_TITLE +"[ "+itemFulfillmentId+" ]"+".afterSubmit -";
            var onDotReponseData = {};
            onDotReponseData.status = '';
            onDotReponseData.message = '';

            let repush = itemFulfillmentRec.getValue('custbody_wd_ondot_repush');
            let isRepushAction = (context.type == context.UserEventType.XEDIT && repush)
            let ondotStatus = itemFulfillmentRec.getValue('custbody_wdff_if_ondot_status');
            let ondotMessage = itemFulfillmentRec.getValue('custbody_wdff_if_ondot_message');
            let shippingMethod = itemFulfillmentRec.getValue({fieldId:'shipmethod'});
            let shipMethodText = itemFulfillmentRec.getText({fieldId:'shipmethod'});
            let location = isRepushAction ? (context.oldRecord).getValue("custbody_wd_source_location") : itemFulfillmentRec.getValue('custbody_wd_source_location');
            ondotMessage = isRepushAction ?  (context.oldRecord).getValue("custbody_wdff_if_ondot_message") : ondotMessage;   //todo: set because of "ship now" button
            ondotStatus = isRepushAction ?   (context.oldRecord).getValue("custbody_wdff_if_ondot_status") : ondotStatus ;    //todo: set because of "ship now" button
            shippingMethod = isRepushAction ?  (context.oldRecord).getValue("shipmethod") : shippingMethod;
            shipMethodText =  isRepushAction ? (context.oldRecord).getText("shipmethod") :shipMethodText;

            let memo = itemFulfillmentRec.getValue('memo');

            let itemFulfillmentLoaded = record.load({type: record.Type.ITEM_FULFILLMENT, id:itemFulfillmentId,isDynamic: true});
            let setTheStatusesOnIf = false;

            log.emergency(logTitle,
                {
                    startTime:Date.now(),
                    location:location,
                    memo:memo,
                    repush:repush,
                    shippingMethod:shippingMethod,
                    contextType: context.type,
                    itemFulfillmentId:itemFulfillmentId,
                    ondotStatus: ondotStatus,
                    ondotMessage:ondotMessage,
                    shipMethodText:shipMethodText

            });
            log.emergency(logTitle,
                {
                    itemFulfillmentLoaded:itemFulfillmentLoaded,


                });
            log.emergency(logTitle,
                {
                    itemFulfillmentRec:itemFulfillmentRec,


                });


            try {
                let paramsMap = params.getParameters(PARAMS);1
                let applicableLocation = paramsMap.custscript_wd_applicable_locations;
                let waveTrigger = paramsMap.custscript_wd_wave_trigger;
                let configId = paramsMap.custscript_wdff_ondot_configuration;
                let useProdUrl = paramsMap.custscript_wdff_ue_use_prod_urls;
                let overrideEnvironmentSetting = paramsMap.custscript_wdff_ue_override_setting;
                let connection = config.getConfig(configId, useProdUrl, overrideEnvironmentSetting);
                let carrierMapping = ondotLib.getOndotCarrierMapping(shippingMethod)

                log.debug(logTitle , {carrierMapping:carrierMapping, connection: JSON.stringify(connection),waveTrigger:waveTrigger});


                if(( context.type != context.UserEventType.XEDIT ) && (applicableLocation != location)){
                    log.error(logTitle , {message: "Unsupported location!", endTime :Date.now()}) ;
                    return;
                }

                if(( context.type != context.UserEventType.XEDIT &&  isEmpty(carrierMapping) /*DHL_SHIPMETHOD_IDS.indexOf(shippingMethod) == -1 && HERMES_SHIPMETHOD_IDS.indexOf(shippingMethod) == -1*/)){
                    log.error(logTitle , {message: "Unsupported carrier!", endTime :Date.now()}) ;
                    return;
                }
                if(connection.testMode && shipMethodText.trim() != (connection.testShipMethod).trim()){ //only for testing purposes
                    log.error(logTitle , {message: "Unsupported carrier! (TEST MODE) ! ", endTime :Date.now()}) ;
                    return;
                }

                //todo testing sending shipment to ondot
                if(!repush && (context.type == context.UserEventType.CREATE)){// /*|| memo == "testLZ"*/
                    ondotLib.exportShipmentToOndot(itemFulfillmentRec,connection,onDotReponseData,carrierMapping);
                    setTheStatusesOnIf = true;

                }else if(!repush){
                    let itemFulfillmentRecOld = context.oldRecord;
                    let oldStatus = itemFulfillmentRecOld.getValue("shipstatus")
                    let newStatus = itemFulfillmentRec.getValue("shipstatus")

                    if((oldStatus == PICKED_STATUS && newStatus == PACKED_STATUS)){
                        ondotLib.exportColloDataToOndot(connection,itemFulfillmentId,itemFulfillmentLoaded,shippingMethod,onDotReponseData,carrierMapping)
                        setTheStatusesOnIf = true;
                    }
                }else if(repush){
                    setTheStatusesOnIf = true;
                    ondotLib.exportShipmentAndCollos(connection,itemFulfillmentLoaded,shippingMethod,onDotReponseData,carrierMapping)
                }

            } catch (e) {

                log.error({title: logTitle, details: e.name + ': ' + e.message});
                onDotReponseData.status = ERROR_STATUS_ID;
                onDotReponseData.message = e.message

            } finally {
                if(setTheStatusesOnIf){
                    itemFulfillmentLoaded.setValue({fieldId:"custbody_wdff_if_ondot_status" , value: (!isEmpty(onDotReponseData.status) ? onDotReponseData.status :  ondotStatus) });
                    itemFulfillmentLoaded.setValue({fieldId:"custbody_wdff_if_ondot_message" , value: (!isEmpty(onDotReponseData.message) ? onDotReponseData.message : ondotMessage) });
                    itemFulfillmentLoaded.setValue({fieldId:"custbody_wd_ondot_timestamp" , value: (!isEmpty(onDotReponseData.status) ? new Date() :  "") });

                    itemFulfillmentLoaded.save();
                }else{
                    log.emergency(logTitle , {message: "Statuses[Ondot] not set on IF! ", endTime :Date.now()}) ;
                }

            }

            log.emergency(logTitle, "END TIME: " + Date.now())
        }

        /*********************************************** CUSTOM FUNCTIONS **********************************************************************/


        function isEmpty(data) {
            return ( data == '' || data == null || (typeof data == 'undefined') || (data.constructor === Object && Object.keys(data).length === 0))
        }



        /**************************************************************************************************************************************/

        return {afterSubmit}

    });
