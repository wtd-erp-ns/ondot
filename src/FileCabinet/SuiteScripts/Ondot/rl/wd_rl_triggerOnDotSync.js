/**
 *@NApiVersion 2.1
 *@NScriptType restlet
 */
define(
    [
        "require",
        "exports",
        "../../3pl-integration/lib/params",
        "N/record",
        "N/search",
        "N/task",
        "../lib/ondot_lib"
    ],
    function (require, exports) {

        const params = require("../../3pl-integration/lib/params");
        const record = require("N/record");
        const search = require("N/search");
        const task = require("N/task")
        const ondotLib = require("../lib/ondot_lib");
        var LOG_TITLE = 'WD_WDFF_RL_Ondot';

        var PARAMS = [
            {
                id: 'custscript_wdff_mr_script_id',
                isMandatory: true
            }
        ]

        return {

            post: function (requestBody) {
                let logTitle = LOG_TITLE + ".post - ";
                log.audit(logTitle);
                log.debug(logTitle, 'requestBody: ' + JSON.stringify(requestBody));
                let isWaveEligibleForProcessing;
                let logStatus = ondotLib.waveStatuses.success;
                let waveNumber = requestBody.params.aux_Params.multiOrderPicking_selectWave_waveTable.wavename;

                try {

                    isWaveEligibleForProcessing = isWaveEligible(waveNumber)
                    if (isWaveEligibleForProcessing) {
                        callMr(waveNumber);
                    } else {
                        log.audit(logTitle, " Wave: " + waveNumber + ", was already processed, shippingNET integration not triggered!");
                    }

                } catch (e) {
                    logStatus = ondotLib.waveStatuses.failed;
                    log.error('Error', (e));

                } finally {

                    // Create wave log
                    if(isWaveEligibleForProcessing){
                        createWaveLogStatus(waveNumber,logStatus);
                    }

                    // Here I would put logic that submits success/error message to custom field on wave
                }

            }

        };

        function callMr(waveNumber) {
            let logTitle = LOG_TITLE + ".callMr - ";
            log.debug(logTitle + " - input data, wave: ", waveNumber);
            let paramsMap = params.getParameters(PARAMS);
            let mrScriptId = paramsMap.custscript_wdff_mr_script_id;
            let mrParams = {
                custscript_wdff_wave_id: waveNumber
            }

            let objTask = task.create({
                taskType: task.TaskType.MAP_REDUCE,
                scriptId: mrScriptId,
                params: mrParams
            });

            let taskId = objTask.submit();
            log.audit(logTitle, 'Process Task for Wave : ' + waveNumber+' submitted: ' + taskId);


        }

        function isWaveEligible(waveNumber) {
            let logTitle = LOG_TITLE + ".isWaveEligible - ";
            log.audit(logTitle);
            let isWaveEligibleForProcessing = true;

            var waveStatusSearch = search.create({
                type: "customrecord_wdff_wave_integrationstatus",
                filters:
                    [
                        ["custrecord_wdff_status","anyof", ondotLib.waveStatuses.success],
                        "AND",
                        ["custrecord_wdff_wave_number","is",waveNumber]
                    ],
                columns:
                    [
                        "internalid"
                    ]
            });

            waveStatusSearch.run().each(function(result){
                let waveStatusLogId = result.getValue({name: "internalid"})
                log.debug(logTitle + "Wave status found: " + waveStatusLogId);
                isWaveEligibleForProcessing = false;
                return true;
            });

            return isWaveEligibleForProcessing;
        }

       function createWaveLogStatus(waveNumber,logStatus){
           let logTitle = LOG_TITLE + ".createWaveLogStatus - ";
           log.audit(logTitle);

           var waveLogRec = record.create({type:"customrecord_wdff_wave_integrationstatus"});
           waveLogRec.setValue("custrecord_wdff_wave_number",waveNumber);
           waveLogRec.setValue("custrecord_wdff_status",logStatus);
           let waveStatusLogId = waveLogRec.save();

           log.debug(logTitle, " Wave log created: " + waveStatusLogId);
           return waveStatusLogId;
       }

    });
