/**
 *  * Module Description:
 *
 * Version       Date              Author                Remarks
 * 1.0          9 Feb 2021         Lucia Zacharova       Initial version
 *
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 */
define(["require", "exports", "N/https", "N/record", 'N/query', "../../3pl-integration/lib/config", "../../3pl-integration/lib/params", "../../3pl-integration/lib/ilog"],
    (require, exports) => {
        const https = require("N/https");
        const record = require("N/record");
        const query = require("N/query");
        const config = require("../../3pl-integration/lib/config");
        const params = require("../../3pl-integration/lib/params");
        const ilog = require("../../3pl-integration/lib/ilog");


        var LOG_TITLE = 'WD_WDFF_Ondot';
        var PARAMS = [
            {
                id: 'custscript_wdff_wave_id',
                isMandatory: true
            },
            {
                id: 'custscript_wdff_configuration',
                isMandatory: true
            },
            {
                id: 'custscript_wdff_override_setting',
                isMandatory: false
            },
            {
                id: 'custscript_wdff_use_prod_urls',
                isMandatory: false
            },
        ];

        exports.getInputData = (context) => {
            let logTitle = LOG_TITLE + ".getInputData - ";
            log.audit(logTitle);
            // get parameters
            let paramsMap = params.getParameters(PARAMS);
            let waveIdToProcess = paramsMap.custscript_wdff_wave_id;
            let configId = paramsMap.custscript_wdff_configuration;
            let useProdUrl = paramsMap.custscript_wdff_use_prod_urls;
            let overrideEnvironmentSetting = paramsMap.custscript_wdff_override_setting;
            let objConnection = {};
            objConnection.config = config.getConfig(configId, useProdUrl, overrideEnvironmentSetting);
            let tranToProcess = getSalesOrdersFromWave(waveIdToProcess);
            //adding config  file
            Object.keys(tranToProcess).forEach((key) => {
                tranToProcess[key].connection = objConnection;
            });
            log.debug(logTitle + " - SOs To Process - ", JSON.stringify(tranToProcess))
            return tranToProcess

        }

        exports.reduce = (context) => {
            let logTitle = LOG_TITLE + ".reduce -";
            let soData = JSON.parse(context.values);
            log.audit(logTitle);
            getSoDetails(soData);
            let response;
            let url = soData.connection.config.baseUrl + soData.connection.config.importOrder;
            let headers = getHeaders(soData.connection.config);
            let body = JSON.stringify(mapData(soData))
            log.debug(logTitle, "body: " + body);
            let logStatus = ilog.LOG_STATE.NEW;

            try {
                response = https.post({
                    url: url,
                    body: body,//JSON.stringify(getTestData(soData.soNumber, 888));
                    headers: headers
                });
                //code 503 == service unavailable
                log.audit(logTitle, "response: " + JSON.stringify(response));
                logStatus = ilog.LOG_STATE.SUCCESS;//todo: success only with code 200

            } catch (error) {
                log.error({title: logTitle, details: exception.name + ': ' + exception.message});
                logStatus = ilog.LOG_STATE.ERROR;
            } finally {

                let logData = {};
                logData.transaction = soData.soId
                logData.url = url;
                logData.request = body;
                logData.response = response;
                logData.code = response.code;
                logData.interface = ilog.INTERFACES.SN01;
                logData.state = logStatus;

                // Create integration log
                logId = ilog.logIntegration(logData);
                log.debug(logTitle, " Integration log created: " + logId);
            }


        }

        exports.summarize = (summary) => {
            let logTitle = LOG_TITLE + ".summarize -";
            log.audit(logTitle);
            var objErrors = {};

            summary.reduceSummary.errors.iterator().each(function (key, error) {
                log.error({
                    title: logTitle + " - REDUCE ERRORS",
                    details: {
                        key: key,
                        error: error.toString()
                    }
                });
                objErrors[key] = {
                    "error": error
                };
                return true;
            });

        }

        /************** CUSTOM FUNCTIONS ************************/

        function getHeaders(configData) {
            let logTitle = LOG_TITLE + ".getHeaders -";
            log.audit(logTitle);
            let headers = {
                "Content-Type": "application/json",
                "auth-token": configData.clientSecret,
                "client-id": configData.clientId,
                "orgunit-id": configData.orgUnitId
            };
            return headers;
        }

        function getSalesOrdersFromWave(waveIdToProcess) {
            let logTitle = LOG_TITLE + ".getSalesOrdersFromWave -";
            log.audit(logTitle);
            let soMap = {};
            let hermesFilter = "HERMES AT"; //1379;//todo: vyfiltrovane podla hermes!!!
            // SuiteQL

            var strSQL = `
            SELECT
              "TRANSACTION".tranid AS wavenumber,
              orderLine_SUB.tranid_0 AS sonumber,
              orderLine_SUB.id_0_0 AS soid,
              BUILTIN.DF(orderLine_SUB."ORDER") AS lineitemsorderDISPLAY /*{lineitems.order#DISPLAY}*/,
              orderLine_SUB.linesequencenumber_0 AS solineid,
              orderLine_SUB.quantity AS solineqty,
              UPPER(orderLine_SUB.shippingmethod_crit_0) AS shippingmethod
            FROM
              "TRANSACTION",
              (SELECT
                orderLine.wave AS wave,
                orderLine.wave AS wave_join,
                transactionLine_SUB.tranid AS tranid_0,
                transactionLine_SUB.id_0 AS id_0_0,
                orderLine."ORDER" AS "ORDER",
                transactionLine_SUB.linesequencenumber AS linesequencenumber_0,
                orderLine.quantity AS quantity,
                transactionLine_SUB.shippingmethod_crit AS shippingmethod_crit_0
              FROM
                orderLine,
                (SELECT
                  transactionLine."TRANSACTION" AS "TRANSACTION",
                  transactionLine."ID" AS "ID",
                  transactionLine."TRANSACTION" AS transaction_join,
                  transactionLine."ID" AS id_join,
                  transaction_0.tranid AS tranid,
                  transaction_0."ID" AS id_0,
                  transactionLine.linesequencenumber AS linesequencenumber,
                  TransactionShipment.shippingmethod AS shippingmethod_crit
                FROM
                  transactionLine,
                  TransactionShipment,
                  "TRANSACTION" transaction_0
                WHERE
                  transaction_0."ID" = TransactionShipment.doc(+)
                   AND transactionLine."TRANSACTION" = transaction_0."ID"
                ) transactionLine_SUB
              WHERE
                orderLine."ORDER" = transactionLine_SUB."TRANSACTION"(+)
                 AND orderLine.line = transactionLine_SUB."ID"(+)
              ) orderLine_SUB
            WHERE
              "TRANSACTION"."ID" = orderLine_SUB.wave(+)
               AND ((UPPER("TRANSACTION".tranid) = '${waveIdToProcess}'))
            `;
            log.debug("strSQL",strSQL)

            // Paged execution
            var objPagedData = query.runSuiteQLPaged({
                query: strSQL,
                pageSize: 1000
            });

            // Paging
            var arrResults = [];
            objPagedData.pageRanges.forEach(function (pageRange) {
                var objPage = objPagedData.fetch({index: pageRange.index}).data;

                // Map results to columns
                arrResults.push.apply(arrResults, objPage.asMappedResults());
            });
            log.audit(logTitle, "arrResults:" + JSON.stringify(arrResults));

            for (var i in arrResults) {
                let searchResult = arrResults[i]
                let lineData = {};
                let soId = searchResult.soid;
                lineData.lineId = searchResult.solineid;
                lineData.lineQty = Math.abs(searchResult.solineqty);

                if (typeof soMap[soId] === 'undefined') {
                    soMap[soId] = {};
                    soMap[soId].lines = [];
                }
                soMap[soId].soId = soId;
                soMap[soId].soNumber = searchResult.sonumber;
                //soMap[soId].waveId = searchResult.waveid;
                soMap[soId].wavenumber = searchResult.wavenumber
                soMap[soId].uniqueIdentificator = searchResult.wavenumber + "_" + searchResult.sonumber
                soMap[soId].shippingmethod = searchResult.shippingmethod;
                soMap[soId].lines.push(lineData);
            }

            return soMap;
        }


        function mapData(soData) {
            let logTitle = LOG_TITLE + ".mapData -";
            log.debug(logTitle);
            let timeStamp = new Date();

            log.debug(logTitle, JSON.stringify(soData));

            let bpn;
            let bpn2;
            let dc;
            let xdc;
            let ds;
            let fp;

            if(soData.shippingmethod.indexOf('DHL') != -1)
            {
              bpn = 'DHL.REGENSBURG';
              bpn2 = 'PROLOGIS';
              dc = 'DHL';
              xdc = 'DHL';
              ds = 'DHL Europaket';
            }
            else if(soData.shippingmethod.indexOf('HERMES') != -1)
            {
              bpn = 'HQ';
              bpn2 = 'PROLOGIS';
              dc = 'HERMES';
              xdc = 'HERMES'
              ds = 'Standard'
            }

            let shipmentData = {
                "Shipment": {
                    "Shipper": {
                        "BusinessPartnerNumber": bpn
                    },
                    "LoadingPoint": {
                        "BusinessPartnerNumber": bpn2
                    },
                    "Recipient": {
                        "Name": soData.recipientInfo.recipient,
                        "Address": {
                            "Name1": soData.recipientInfo.recipient,
                            "AddressLine1": soData.recipientInfo.addressLine,
                            "HouseNumber": soData.recipientInfo.houseNumber,
                            "PostalCode": soData.recipientInfo.postalCode,
                            "City": soData.recipientInfo.city,
                            "CountryID": soData.recipientInfo.countryId,
                            "Tel1": soData.recipientInfo.telephone,
                            "Email": soData.recipientInfo.email
                        },
                    },
                    "RoutingType": "Delivery",
                    "DispatchType": "Package",
                    "DeliveryCarrier": dc,//todo: can not be hardcoded
                    "xDeliveryCarrier": xdc,//todo: can not be hardcoded
                    "DeliveryService": ds,
                    "ValueOfGoods": soData.valueOfItems,
                    "ValueOfGoodsCurrencyID": soData.currency,
                    "GoodsDescription": " test import description  - " + timeStamp,
                    "ShipperReference1": "NS-" + soData.soNumber,
                    "ThirdPartyApplicationList": [
                        {
                            "ReferenceID": soData.uniqueIdentificator,
                            "UName": "waterdrop.NS"
                        }
                    ]
                },
                "CreateReturnShipment": false,
                "RequestedFields": {
                    "Shipment": {
                        "Number": true,
                        "DeliveryCarrierID": true
                    }
                }
            }

            if(soData.shippingmethod.indexOf('DHL') != -1)
            {
              shipmentData["Shipment"]["FreightPayer"] = { "BusinessPartnerNumber": "HQ" }
            }

            return shipmentData;
        }

        function getSoDetails(soData) {
            let logTitle = LOG_TITLE + ".getSoDetails -";
            log.debug(logTitle);
            var soRec = record.load({
                type: record.Type.SALES_ORDER,
                id: soData.soId
            });
            var valueOfItems = 0;
            soData.carrier = soRec.getValue('shipmethod');
            soData.currency = soRec.getValue('currencysymbol');
            var shippingAddress = soRec.getSubrecord('shippingaddress');
            soData.recipientInfo = getRecipientInfo(soRec, shippingAddress)

            for (var i in soData.lines) {
                var lineId = parseInt(soData.lines[i].lineId);
                let lineGross = Number(Number(soRec.getSublistValue({
                    sublistId: "item",
                    fieldId: "grossamt",
                    line: (lineId - 1)
                })).toFixed(2));
                soData.lines[i].grossAmt
                valueOfItems += lineGross;
            }
            soData.valueOfItems = Number(valueOfItems.toFixed(2));
            log.debug(logTitle + " - SO details:", JSON.stringify(soData));
        }


        function getRecipientInfo(soRec, shippingAddress) {
            let logTitle = LOG_TITLE + ".getRecipientInfo -";
            log.debug(logTitle);
            var recipientInfo = {};
            recipientInfo.recipient = shippingAddress.getValue('addressee');
            recipientInfo.addressLine = shippingAddress.getValue('addr1');
            recipientInfo.houseNumber = shippingAddress.getValue('custrecord_oro_6950_streetnumber');
            recipientInfo.postalCode = shippingAddress.getValue('zip');
            recipientInfo.city = shippingAddress.getValue('city');
            recipientInfo.countryId = shippingAddress.getValue('country');
            recipientInfo.telephone = shippingAddress.getValue('addrphone');
            recipientInfo.email = soRec.getValue('email');
            return recipientInfo;
        }


        function getTestData(soNumber, valueOfGoods) {
            let logTitle = LOG_TITLE + ".getTestData -";
            log.debug(logTitle);
            let timeStamp = new Date();

            var TEST_DATA = {
                "Shipment": {
                    "Shipper": {
                        "BusinessPartnerNumber": "DHL.REGENSBURG"
                    },
                    "Recipient": {
                        "BusinessPartnerNumber": "TEST.DE"
                    },
                    "RoutingType": "Delivery",
                    "DispatchType": "Package",
                    "DeliveryCarrier": "DHL",
                    "xDeliveryCarrier": "DHL",
                    "DeliveryService": "DHL Paket",
                    "ValueOfGoods": valueOfGoods,
                    "ValueOfGoodsCurrencyID": "EUR",
                    "GoodsDescription": " test description - " + timeStamp,
                    "ThirdPartyApplicationList": [
                        {
                            "ReferenceID": soNumber,
                            "UName": "waterdrop.NS"
                        }
                    ]
                },
                "CreateReturnShipment": false,
                "RequestedFields": {
                    "Shipment": {
                        "Number": true,
                        "DeliveryCarrierID": true
                    }
                }
            }
            return TEST_DATA
        }

        // function isWaveEligible(waveId) {
        //     let logTitle = LOG_TITLE + ".isWaveEligible - ";
        //     log.audit(logTitle);
        //     let eligibleWaveForProcessing = -1;
        //     var waveSearch = search.create({
        //         type: "wave",
        //         filters:
        //             [
        //                 ["type", "anyof", "Wave"],
        //                 "AND",
        //                 ["custbody_wdff_ondot_exp_statuses", "anyof", "@NONE@"],
        //                 "AND",
        //                 ["numbertext", "is", waveId]
        //             ],
        //         columns:
        //             [
        //                 search.createColumn({
        //                     name: "internalid",
        //                     summary: "GROUP"
        //                 }),
        //                 search.createColumn({
        //                     name: "tranid",
        //                     summary: "GROUP"
        //                 })
        //             ]
        //     });
        //
        //     waveSearch.run().each(function (result) {
        //         let waveId = result.getValue({name:"internalid",summary:"GROUP"})
        //         eligibleWaveForProcessing = waveId;
        //         log.debug(logTitle,"Wave: " + isWaveEligibleForProcessing + ", is eligible.");
        //         return true;
        //     });
        //
        //     return eligibleWaveForProcessing;
        // }
        //
        //

        // function getSalesOrdersFromWave(waveIdToProcess){
        //     let logTitle = "<< getSalesOrdersFromWave >>";
        //     let soMap = {};
        //     let waveSearch = search.create({
        //         type: "transaction",
        //         filters:
        //             [
        //                 ["internalid","anyof",waveIdToProcess],
        //                 "AND",
        //                 ["appliedtotransaction.numbertext","isnotempty",""]
        //             ],
        //         columns:
        //             [
        //                 search.createColumn({
        //                     name: "tranid",
        //                     join: "appliedToTransaction"
        //                 }),
        //                 search.createColumn({
        //                     name: "internalid",
        //                     join: "appliedToTransaction"
        //                 }),
        //                 search.createColumn({
        //                     name: "line",
        //                     join: "appliedToTransaction"
        //                 }),
        //                 search.createColumn({
        //                     name: "item",
        //                     join: "appliedToTransaction"
        //                 }),
        //                 search.createColumn({
        //                     name: "quantity",
        //                     join: "appliedToTransaction"
        //                 })
        //             ]
        //     });
        //
        //     let searchResultCount = waveSearch.runPaged().count;
        //     log.debug("waveSearch result count",searchResultCount);
        //
        //     let arrRecSearch = waveSearch.runPaged();
        //     arrRecSearch.pageRanges.forEach(function(pageRange) {
        //         let arrMyPageOfResults = arrRecSearch.fetch({
        //             index: pageRange.index
        //         });
        //         //getting through the saved search results and setting the sublist
        //         arrMyPageOfResults.data.forEach(function(result) {
        //
        //             let objResult = JSON.parse(JSON.stringify(result));
        //             let objLine = {}
        //             let soId = result.getValue({
        //                 name: "internalid",
        //                 join: "appliedToTransaction"
        //             });
        //             let soNumber = result.getValue({
        //                 name: "tranid",
        //                 join: "appliedToTransaction"
        //             });
        //             objLine.lineId = result.getValue({
        //                 name: "line",
        //                 join: "appliedToTransaction"
        //             });
        //             objLine.lineQty = result.getValue({
        //                 name: "quantity",
        //                 join: "appliedToTransaction"
        //             });
        //             if (typeof soMap[soId] === 'undefined') {
        //                 soMap[soId] = {};
        //                 soMap[soId].soNumber = soNumber;
        //                 soMap[soId].lines = [];
        //             }
        //             soMap[soId].lines.push(objLine);
        //
        //
        //         });
        //     });
        //     log.audit(logTitle, "soMap:"+ JSON.stringify(soMap));
        //     return soMap;
        // }

        /************** CUSTOM FUNCTIONS ************************/


    });
