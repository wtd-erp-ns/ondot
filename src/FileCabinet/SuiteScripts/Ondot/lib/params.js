/**
 * Module Description:
 *
 * Version       Date              Author                Remarks
 * 1.0           07 Dec 2020       Libor Nekula        Initial Version
 * 
 * @NApiVersion 2.1
 * @NModuleScope Public
 * 
 */

define(['require', 'exports', 'N/error', 'N/runtime'], function(require, exports) {
    const error = require('N/error');
    const runtime = require('N/runtime');

	/**
     * Get script parameters
     * 
     * @param {Array} parameterList - Array defining expected script parameters	 
     * @returns {object} params - contains map of parameters and their values	 
     */
    exports.getParameters = (parameterList) => {
        let params = {};
        let scriptObj = runtime.getCurrentScript();

        // For each parameter object provided in the list
        parameterList.forEach((currentParameter) => {

            /*
            log.debug({
                title: 'getParameters',
                details: JSON.stringify(currentParameter)
            })

            log.debug({
                title: 'getParameters',
                details: currentParameter.id
            })

            log.debug({
                title: 'getParameters',
                details: currentParameter.isMandatory
            })*/

            // Obtain param id and its value
            let paramValue = scriptObj.getParameter({
                name: currentParameter.id
            });

            // Check for non-empty values for mandatory parameters
            if(currentParameter.isMandatory)
            {
                if(paramValue === '' || paramValue === undefined || paramValue === 'undefined' || paramValue === null)
                {
                    throw error.create({
                        name: "SSS_MANDATORY_PARAMETER_MISSING",
                        message: "No value provided for mandatory parameter "+currentParameter.id,
                        notifyOff: false
                    });
                }
            }

            params[currentParameter.id] = paramValue;
        });

        return params;
	};    
});