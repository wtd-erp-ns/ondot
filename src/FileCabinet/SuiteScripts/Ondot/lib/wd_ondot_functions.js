/**
 * @NApiVersion 2.1
 */
define(["require", "exports",'N/https','N/file',"N/search","N/runtime","../../3pl-integration/lib/ilog","N/record"],
    
    (require, exports) => {
            const https = require("N/https")
            const search = require("N/search");
            const file = require("N/file");
            const runtime = require("N/runtime");
            const ilog = require("../../3pl-integration/lib/ilog");
            const record = require("N/record");

            const prodAccount = true;

            const LOG_TITLE = 'WD_WDFF_Ondot_Send_Cartons - ';
            const ONDOT_ERROR_CARRIER = 1999;
            const ONDOT_DHL_CARRIER_ID = 1227;
            const ONDOT_HERMES_CARRIER_ID = 1380;
            const ONDOT_DPD_CARRIER_ID = 1001;
             const ONDOT_CHRONO_CARRIER_ID = 1389;

            const SUCCESS_STATUS_ID = 1;
            const ERROR_STATUS_ID = 2;
            const PARTIAL_SUCCESS_ID = 3;
            const SHIPMENT_EXPORTED = 4;

            const HERMES_SHIPMETHOD_IDS = prodAccount ? ["1379","2921"] : ["1379","2714"];
            const DHL_SHIPMETHOD_IDS = prodAccount ? ["1373","2961","2922","2917"] : ["1373","2711"];
            const PICKED_STATUS = "A";
            const PACKED_STATUS = "B";


            /*********************************************** CUSTOM FUNCTIONS **********************************************************************/

            //export and post functions
             exports.exportShipmentAndCollos = function(connection,itemFulfillmentLoaded,shippingMethod,onDotReponseData,carrierMapping,emails){
                    let logTitle = LOG_TITLE + ".exportShipmentAndCollos -";
                    log.debug(logTitle);
                    onDotReponseData.status = SUCCESS_STATUS_ID;
                    onDotReponseData.message = '-';
                    itemFulfillmentLoaded.setValue({fieldId:"custbody_wd_ondot_repush" , value:  false});
                    let urlLabelRequest = connection.baseUrl + connection.requestLabel;
                    let urlRetrieveShipment = connection.baseUrl + connection.retrieveShipment;
                    let error = {};
                    let headers = getHeaders(connection);
                    let tranData = getTranDetails(itemFulfillmentLoaded,emails);
                     let waveTrigger = runtime.getCurrentScript().getParameter("custscript_wd_wave_trigger");

                    let cartonData = getCartonData(itemFulfillmentLoaded);
                    let mappedCartonData  = mapCartonDataForRepush(cartonData,carrierMapping);
                    let mappedShipmentData = mapShipmentData(tranData,carrierMapping, true);

                    mappedShipmentData["Shipment"]["ColloList"] = mappedCartonData;
                    mappedShipmentData["RequestedFields"]["Collo"] = {}
                    mappedShipmentData["RequestedFields"]["Collo"]["Number"] = true;
                    mappedShipmentData["RequestedFields"]["Collo"]["TrackingNumber"] = true;

                    let responses = []; responses.push(postShipment(mappedShipmentData,connection,tranData));
                    let responseData = parseColloResponse(responses,onDotReponseData,true);

                    //if the export of cartons/boxes was successfull then request labels and set hte exported boxes on the IF subtab
                    if(responseData.exportedPackages.length != 0){

                            let labelRequestData = mapLabelData(responseData.exportedPackages, shippingMethod,carrierMapping);
                            let labelResponses = saveLabelFiles(labelRequestData,urlLabelRequest,headers,cartonData.itemWeightMap,onDotReponseData);

                            let shipmentId = waveTrigger ? getShipmentReference(tranData.ifId,tranData.soNumber) : tranData.ifId;
                            let trackingNumbersData = retrieveTrackingNumbers(urlRetrieveShipment,headers, shipmentId,onDotReponseData);
                            trackingNumbersData = mergeLabelsAndTrackingNumbers(labelResponses,trackingNumbersData,cartonData.itemWeightMap)
                            setPackagesOnIf(itemFulfillmentLoaded,trackingNumbersData,cartonData.itemWeightMap);
                            saveLabelsOnIf(itemFulfillmentLoaded, labelResponses,trackingNumbersData);
                    }

            }


            exports.exportColloDataToOndot = function (connection,itemFulfillmentId,itemFulfillmentLoaded,shippingMethod,onDotReponseData,carrierMapping){
                    let logTitle = LOG_TITLE + ".exportColloDataToOndot -";
                    let urlExportBoxes = connection.baseUrl + connection.exportBoxes;
                    let urlRetrieveShipment = connection.baseUrl + connection.retrieveShipment;
                    let urlLabelRequest = connection.baseUrl + connection.requestLabel;
                    let waveTrigger = runtime.getCurrentScript().getParameter("custscript_wd_wave_trigger");
                    let error = {};
                    let soNumber = itemFulfillmentLoaded.getValue('custbody_wdff_so_number');
                    let shipmentId = waveTrigger ? getShipmentReference(itemFulfillmentId,soNumber) : itemFulfillmentId//getShipmentReference(itemFulfillmentId,soNumber);
                    onDotReponseData.status = SUCCESS_STATUS_ID;
                    onDotReponseData.message = '-';

                    log.debug({
                            title:logTitle + " - Input data",
                            details:{
                                    urlExportBoxes:urlExportBoxes,
                                    soNumber:soNumber,
                            }
                    });
                    let headers = getHeaders(connection);

                    let cartonData = getCartonData(itemFulfillmentLoaded);
                    let cartonRequestData  = mapCartonData(cartonData,shipmentId);

                    // //in majority of cases there should be only one request
                    let responses = postData(cartonRequestData,urlExportBoxes,headers);
                    responseData = parseColloResponse(responses,onDotReponseData,false);

                    //if the export of cartons/boxes was successfull then request labels and set hte exported boxes on the IF subtab
                    if(responseData.exportedPackages.length != 0){

                            let labelRequestData = mapLabelData(responseData.exportedPackages, shippingMethod,carrierMapping);
                            let labelResponses = saveLabelFiles(labelRequestData,urlLabelRequest,headers,cartonData.itemWeightMap,onDotReponseData);
                            let trackingNumbersData = retrieveTrackingNumbers(urlRetrieveShipment,headers, shipmentId,onDotReponseData);
                            trackingNumbersData = mergeLabelsAndTrackingNumbers(labelResponses,trackingNumbersData,cartonData.itemWeightMap)
                            setPackagesOnIf(itemFulfillmentLoaded,trackingNumbersData,cartonData.itemWeightMap);
                        saveLabelsOnIf(itemFulfillmentLoaded, labelResponses,trackingNumbersData);
                    }
            }

             exports.exportShipmentToOndot = function(itemFulfillmentRec,connection,onDotReponseData,carrierMapping,emails){
                    let logTitle = LOG_TITLE + ".exportShipmentToOndot -";
                    log.debug(logTitle);
                    onDotReponseData.status = SHIPMENT_EXPORTED;
                    onDotReponseData.message = '-';
                    let tranData = getTranDetails(itemFulfillmentRec,emails);
                    let mappedShipmentData = mapShipmentData(tranData,carrierMapping, false);
                    postShipment(mappedShipmentData,connection,tranData)
            }


            exports.getOndotCarrierMapping = function(shipMethod){
                    let logTitle = LOG_TITLE + ".getOndotCarrierMapping -";
                    log.debug(logTitle)
                    let carrierMapping = {};
                    if(isEmpty(shipMethod)){
                        return carrierMapping;
                    }
                    var ondotCarriersMapSearchObj = search.create({
                            type: "customrecord_wd_ondot_carriers",
                            filters:
                                [
                                        ["custrecord_wd_ship_methods","anyof",shipMethod]
                                ],
                            columns:
                                [
                                        "custrecord_wd_ondot_carrier",
                                        "custrecord_wd_ondot_carrier_id",
                                        "custrecord_wd_ship_methods",
                                        "custrecord_wd_bpn",
                                        "custrecord_wd_bpn_2",
                                        "custrecord_wd_delivery_carrier",
                                        "custrecord_wd_xdelivery_carrier",
                                        "custrecord_wd_default_del_service",
                                        "custrecord_wd_label_language",
                                        "custrecord_wd_paper_layout",
                                        "custrecord_wd_freight_payer_bpn"
                                ]
                    });
                    var searchResultCount = ondotCarriersMapSearchObj.runPaged().count;
                    log.debug("ondotCarriersMapSearchObj result count",searchResultCount);
                    ondotCarriersMapSearchObj.run().each(function(result){
                            carrierMapping.carrierId = result.getValue({name:"custrecord_wd_ondot_carrier_id"});
                            carrierMapping.businessPartner = result.getValue({name:"custrecord_wd_bpn"})
                            carrierMapping.businessPartner2 = result.getValue({name:"custrecord_wd_bpn_2"})
                            carrierMapping.deliveryCarrier = result.getValue({name:"custrecord_wd_delivery_carrier"})
                            carrierMapping.xDeliveryCarrier = result.getValue({name:"custrecord_wd_xdelivery_carrier"})
                            carrierMapping.deliveryService = result.getValue({name:"custrecord_wd_default_del_service"})
                            carrierMapping.labelLanguage = result.getValue({name:"custrecord_wd_label_language"})
                            carrierMapping.LabelLayout = result.getValue({name:"custrecord_wd_paper_layout"})
                            carrierMapping.shipMethods = result.getValue({name:"custrecord_wd_ship_methods"})
                            carrierMapping.freightPayer = result.getValue({name:"custrecord_wd_freight_payer_bpn"})
                            return false;
                    });
                    return carrierMapping;
            }

            ////////////////////// CUSTOM FUNCTIONS //////////////////////////////////

            function postShipment(shipmentBody,connection,tranData){
                    let logTitle = LOG_TITLE + ".postShipment -";
                    let url = connection.baseUrl + connection.importOrder;
                    let headers = getHeaders(connection);
                    let response;
                    //shipmentBody = JSON.stringify(shipmentBody);
                    log.audit(logTitle, "shipmentBody: " + JSON.stringify(shipmentBody));
                    let logStatus = ilog.LOG_STATE.NEW;

                    try {
                            response = https.post({
                                    url: url,
                                    body: JSON.stringify(shipmentBody),
                                    headers: headers
                            });
                            //code 503 == service unavailable
                            log.audit(logTitle, "response: " + JSON.stringify(response));
                            logStatus = ilog.LOG_STATE.SUCCESS;

                    } catch (error) {
                            log.error({title: logTitle, details: exception.name + ': ' + exception.message});
                            logStatus = ilog.LOG_STATE.ERROR;
                    } finally {

                            let logData = {};
                            logData.transaction = tranData.ifId
                            logData.url = url;
                            logData.request = JSON.stringify(shipmentBody);
                            logData.response = JSON.stringify(response);
                            logData.code = response.code;
                            logData.interface = ilog.INTERFACES.SN01;
                            logData.state = logStatus;

                            // Create integration log
                            logId = ilog.logIntegration(logData);
                            log.debug(logTitle, " Integration log created: " + logId);
                    }
                    return response;
            }

            function retrieveTrackingNumbers(url,headers,shipmentId,onDotReponseData){
                    let logTitle = LOG_TITLE + ".retrieveTrackingNumbers -";
                    let packagesData = [];
                    log.debug(logTitle);
                    let body = {
                            "ShipmentMatching": {
                                    "ThirdPartyApplicationReference": shipmentId
                            },
                            "RequestedFields": {
                                    "Shipment": {
                                            "Codes": true,
                                            "Status": true,
                                            "Number": true,
                                            "DeliveryCarrierID": true,
                                            "PickupCarrierID": true
                                    },
                                    "Collo": {
                                            "Codes": true,
                                            "Status": true,
                                            "TrackingLink": true,
                                            "Number": true,
                                            "TrackingNumber": true
                                    }
                            }
                    }
                    let response = https.post({
                            url: url,
                            body: JSON.stringify(body),
                            headers: headers
                    });
                    log.audit({
                            title: logTitle + "- response",
                            details: {
                                    response:JSON.stringify(response),
                            }
                    });
                    if(response.code == 200){
                            responseBody = JSON.parse(response.body);
                            getErrorMessage(responseBody,onDotReponseData)
                            packagesData = responseBody.ColloList
                             log.debug(logTitle,{packagesData:packagesData})
                            packagesData = packagesData.reduce(function(map, obj) {
                                    map[obj.Number] = {TrackingNumber: obj.TrackingNumber, TrackingLink: obj.TrackingLink,StatusList: obj.StatusList,CodeList:obj.CodeList};
                                    return map;
                            }, {});

                    }
                    return packagesData;
            }

            function postData(bodyData,urlRequest,headers){
                    let logTitle = LOG_TITLE + ".postData -";
                    log.debug(logTitle);
                    log.audit({
                            title: logTitle + "Input data",
                            details:{
                                    bodyData:bodyData
                            }
                    })
                    let responses = [];
                    for(let i in bodyData){
                            response = https.post({
                                    url: urlRequest,
                                    body: JSON.stringify(bodyData[i]),//JSON.stringify(getTestData(tranData.soNumber, 888));
                                    headers: headers
                            });
                            //code 503 == service unavailable
                            // log.debug(logTitle, "response: " + JSON.stringify(response));
                            responses.push(response)
                    }

                    return responses;
            }


            //get functions
            function getTranDetails(itemFulfillmentRec,emails){
                    let logTitle = LOG_TITLE + ".getTranDetails -";
                    log.debug(logTitle);
                    let tranData = {}
                    tranData.shippingmethod = (itemFulfillmentRec.getValue('shipmethod'));
                    tranData.currency = itemFulfillmentRec.getValue('currencycode');
                    tranData.soNumber = itemFulfillmentRec.getValue('custbody_wdff_so_number');
                    tranData.valueOfGoods = itemFulfillmentRec.getValue('custbody_wd_so_total');
                    tranData.ifNumber = itemFulfillmentRec.getValue('tranid');
                    tranData.ifId = itemFulfillmentRec.getValue('id');
                    tranData.shippingAddress = itemFulfillmentRec.getSubrecord('shippingaddress');
                    tranData.recipientInfo = getRecipientInfo(itemFulfillmentRec, tranData.shippingAddress,emails);
                    return tranData
            }

            function getRecipientInfo(tranRec, shippingAddress,emails) {
                    let logTitle = LOG_TITLE + ".getRecipientInfo -";
                    log.debug(logTitle);
                    let recipientInfo = {};
                    recipientInfo.attention = shippingAddress.getValue('attention');
                    recipientInfo.recipient = shippingAddress.getValue('addressee');
                    recipientInfo.addressLine = shippingAddress.getValue('custrecord_oro_6950_streetname');
                    recipientInfo.addressLine2 = shippingAddress.getValue('addr2');
                    recipientInfo.houseNumber = shippingAddress.getValue('custrecord_oro_6950_streetnumber');
                    recipientInfo.postalCode = shippingAddress.getValue('zip');
                    recipientInfo.provinceCode = tranRec.getValue('custbody_wd_province_it');
                    recipientInfo.city = shippingAddress.getValue('city');
                    recipientInfo.countryId = shippingAddress.getValue('country');
                    recipientInfo.telephone = shippingAddress.getValue('addrphone');
                    recipientInfo.email = tranRec.getValue('custbody_wd_cust_email');//!!(tranRec.getValue('custbody_wd_cust_email')) ? tranRec.getValue('custbody_wd_cust_email') : emails;

                if (!!!(recipientInfo.email)) {
                    if(tranRec.getValue('custbody_wd_createdfrom_rec_type') == 'Transfer Order'){
                        recipientInfo.email = !!emails.fallbackLocationEmail ? emails.fallbackLocationEmail : 'lukasvrabec@wdff.cz';
                    }else{
                        recipientInfo.email = emails.fallbackEmailForSo//'info@waterdrop.com';
                    }

                }

                    return recipientInfo;
            }


            function getCartonData(itemFulfillmentRec){
                    let logTitle = LOG_TITLE + ".getCartonData -";
                    log.audit(logTitle );
                    let returnedValues = {}
                    returnedValues.cartons = [];
                    returnedValues.itemWeightMap = {};
                    returnedValues.itemDescriptionMap = {};
                     returnedValues.shippingBoxMap = {};
                     returnedValues.totalWeight = 0;
                    let subListCount = itemFulfillmentRec.getLineCount("item");

                    for (let i = 0; i < subListCount; i++) {
                            let packCarton;
                            let lineNum = itemFulfillmentRec.selectLine({sublistId: "item", line: i });

                            let itemQty = parseInt(itemFulfillmentRec.getCurrentSublistValue({sublistId: 'item', fieldId: 'quantity'}));
                            let itemName = (itemFulfillmentRec.getCurrentSublistValue({sublistId: 'item', fieldId: 'itemdescription'}));
                            let shippingBox = (itemFulfillmentRec.getCurrentSublistValue({sublistId: 'item', fieldId: 'custcol_wd_shipbox_item'}));
                            let shippingBoxWidth = (itemFulfillmentRec.getCurrentSublistValue({sublistId: 'item', fieldId: 'custcol_wd_item_width'}));
                            let shippingBoxLength = (itemFulfillmentRec.getCurrentSublistValue({sublistId: 'item', fieldId: 'custcol_wd_item_length'}));
                            let shippingBoxHeight = (itemFulfillmentRec.getCurrentSublistValue({sublistId: 'item', fieldId: 'custcol_wd_item_height'}));
                            let itemWeightUnit = (itemFulfillmentRec.getCurrentSublistValue({sublistId: 'item', fieldId: 'custcol_wd_item_weight_unit'}));
                            let weightMultiplier = getWeightToKgMultiplier(itemWeightUnit);
                            let lineItemWeight = itemFulfillmentRec.getCurrentSublistValue({sublistId: 'item', fieldId: 'custcol_wdff_item_weight'})
                            let itemWeight = parseFloat((parseFloat(lineItemWeight) * weightMultiplier * itemQty).toFixed(2)); //todo
                            let invDetailSubRecord = itemFulfillmentRec.getCurrentSublistSubrecord({sublistId: 'item', fieldId: 'inventorydetail'});
                            let subListCountInvDet = invDetailSubRecord.getLineCount({sublistId: 'inventoryassignment'});
                       //     log.debug(logTitle + " - invDetailSubRecord",JSON.stringify(invDetailSubRecord));
                            for(let j = 0; j < subListCountInvDet; j++){
                                    let lineNum = invDetailSubRecord.selectLine({sublistId: "inventoryassignment", line: j});
                                    packCarton = invDetailSubRecord.getCurrentSublistValue({sublistId: 'inventoryassignment', fieldId: 'packcarton'});
                                    packCarton  = !isEmpty(packCarton)? packCarton : invDetailSubRecord.getCurrentSublistValue({sublistId: 'inventoryassignment', fieldId: 'custrecord_wmsse_packing_container'}) ;
                                    let packageQty = invDetailSubRecord.getCurrentSublistValue({sublistId: 'inventoryassignment', fieldId: 'quantity'});
                                    let packageWeight = round2DecPlaces(parseFloat(parseFloat(lineItemWeight) * packageQty * weightMultiplier));

                                    if(!isEmpty(packCarton) && returnedValues.cartons.indexOf(packCarton) < 0){
                                            returnedValues.cartons.push(packCarton);
                                    }
                                if(typeof returnedValues.itemWeightMap[packCarton] == "undefined"){
                                    returnedValues.itemWeightMap[packCarton] =  packageWeight ;

                                }else{
                                    returnedValues.itemWeightMap[packCarton] += packageWeight;

                                }
                                if(typeof returnedValues.itemDescriptionMap[packCarton] == "undefined"){
                                    returnedValues.itemDescriptionMap[packCarton] = itemName + ";";
                                }else{
                                    returnedValues.itemDescriptionMap[packCarton] += itemName + ";";
                                }
                                if(typeof returnedValues.shippingBoxMap[packCarton] == "undefined"){
                                    returnedValues.shippingBoxMap[packCarton] = {};
                                    returnedValues.shippingBoxMap[packCarton].shippingBox = shippingBox;
                                    returnedValues.shippingBoxMap[packCarton].shippingBoxWidth = shippingBoxWidth != "" ? shippingBoxWidth : 1;
                                    returnedValues.shippingBoxMap[packCarton].shippingBoxLength = shippingBoxLength != "" ? shippingBoxLength : 1;
                                    returnedValues.shippingBoxMap[packCarton].shippingBoxHeight = shippingBoxHeight != "" ? shippingBoxHeight : 1;
                                }
                            }

                            returnedValues.totalWeight += itemWeight

                    }
                    log.debug(logTitle + " - returnedValues",returnedValues);

                    if(returnedValues.cartons.length == 0){
                            let error = {}
                            error.message = "Packages/Pack Cartons not found on lines!"
                            throw error;
                    }

                    let associatedCartons = itemFulfillmentRec.getValue("custbody_wd_associated_cartons")
                    associatedCartons = associatedCartons != "" ? associatedCartons.split(";") : [];

                    for(let k in returnedValues.cartons){
                        if(associatedCartons.indexOf(returnedValues.cartons[k]) === -1){
                            associatedCartons.push(returnedValues.cartons[k])
                        }
                    }

                    log.debug({title:logTitle,details:{associatedCartons:associatedCartons}})
                    itemFulfillmentRec.setValue("custbody_wd_associated_cartons",associatedCartons.join(";"))

                    return returnedValues;
            }

            function getHeaders(configData) {
                    let logTitle = LOG_TITLE + ".getHeaders -";
                  //  log.audit(logTitle);
                    let headers = {
                            "Content-Type": "application/json",
                            "auth-token": configData.clientSecret,
                            "client-id": configData.clientId,
                            "orgunit-id": configData.orgUnitId
                    };
                    return headers;
            }

            function getWeightToKgMultiplier(itemWeightUnit){
                    let logTitle = LOG_TITLE + ".getWeightToKgMultiplier -";
                 //   log.audit(logTitle,{"itemWeightUnit":itemWeightUnit});

                    let multiplier = 1;
                    if(itemWeightUnit == 'g'){
                            multiplier = 0.001;
                    }else if(itemWeightUnit == 'lb' ){
                            multiplier = 0.45359
                    }else if(itemWeightUnit == 'oz'){
                            multiplier = 0.028349
                    }
                    return multiplier;
            }

            function getAssociatedLabelData(colloList,labelsData,itemWeightMap){
                    let logTitle = LOG_TITLE + ".getAssociatedLabelData -";
                    let returnedValue = {}
                    let foundWeight = 1; //default value set here
                    colloList.forEach((barCode) => {
                            log.debug(logTitle,{barCode:barCode,labelsData:labelsData})
                            if(typeof labelsData[barCode.Code] !== 'undefined'){
                                    returnedValue =  labelsData[barCode.Code];
                            }
                            if(typeof itemWeightMap[barCode.Code] !== 'undefined'){
                                foundWeight = itemWeightMap[barCode.Code]
                            }


                    });
                    returnedValue.weight = foundWeight;
                    return returnedValue;
            }

            function getErrorMessage(responseBody,onDotReponseData){
                    let logTitle = LOG_TITLE + ".getErrorMessage -";
                    log.debug(logTitle +" - Input Data:" ,
                        {
                                Shipment: JSON.stringify(responseBody.Shipment),
                                StatusList: JSON.stringify(responseBody.Shipment.StatusList),
                                deliveryCarrier:responseBody.Shipment.DeliveryCarrierID
                        });
                    if(responseBody.Shipment.DeliveryCarrierID == ONDOT_ERROR_CARRIER){
                            packagesData = responseBody.ColloList;
                            for(let i in packagesData.StatusList){
                                    if(packagesData.StatusList[i].StatusID == "ERR"){
                                            onDotReponseData.status = PARTIAL_SUCCESS_ID;
                                            onDotReponseData.message += packagesData.StatusList[i].Description + ";"

                                    }
                            }
                            for(let i in responseBody.Shipment.StatusList){
                                    if(responseBody.Shipment.StatusList[i].StatusID == "ERR"){
                                            onDotReponseData.status = PARTIAL_SUCCESS_ID;
                                            onDotReponseData.message += responseBody.Shipment.StatusList[i].Description + ";"
                                    }
                            }
                    }

                    log.audit(logTitle,"Errors found:" + onDotReponseData.message);
            }

            //map functions
            function mapCartonData(cartonData, shipmentId){
                    let logTitle = LOG_TITLE + ".mapCartonData -";
                 //   log.audit(logTitle);
                    let cartons = cartonData.cartons;
                    let mappedCartonsData = [];
                    let timeStamp = new Date();
                for(let i in cartons){
                            let mappedCarton  =   {
                                    "Collo": {
                                            "DisplayNumber": cartons[i],
                                            "PackageTypeID": "PC",
                                            "Weight": cartonData.itemWeightMap[cartons[i]],
                                            "Length": cartonData.shippingBoxMap[cartons[i]].shippingBoxLength,
                                            "Width": cartonData.shippingBoxMap[cartons[i]].shippingBoxWidth,
                                            "Height": cartonData.shippingBoxMap[cartons[i]].shippingBoxHeight,
                                            // "Reference1": "test",
                                            // "Reference2": "test",
                                            "GoodsDescription": cartonData.itemDescriptionMap[cartons[i]]
                                    },
                                    "ShipmentMatching": {
                                            "ThirdPartyApplicationReference": shipmentId
                                    },
                                    "RequestedFields": {
                                            "Collo": {
                                                    "Number": true,
                                                    "TrackingNumber": true
                                            }
                                    }
                            }
                            mappedCartonsData.push(mappedCarton)
                    }

                    return mappedCartonsData;

            }

            function mapCartonDataForRepush(cartonData,carrierMapping){
                    let logTitle = LOG_TITLE + ".mapCartonDataForRepush -";
                   // log.audit(logTitle);
                    let cartons = cartonData.cartons;
                    let mappedCartonsData = [];
                    let timeStamp = new Date();
                    let isDhl = parseInt(carrierMapping.carrierId) == ONDOT_DHL_CARRIER_ID;
                    let isWeihtValidForDhl = parseInt(carrierMapping.carrierId) == ONDOT_DHL_CARRIER_ID && cartonData.totalWeight  < 31.5;
                    let dhlWeight = !isWeihtValidForDhl ? Math.floor((31.5/cartons.length)):0
                   log.debug(LOG_TITLE,{isWeihtValidForDhl:isWeihtValidForDhl,dhlWeight:dhlWeight,totalWeight:cartonData.totalWeight})
                    for(let i in cartons){
                            let mappedCarton  =    {
                                    "DisplayNumber": cartons[i],
                                    "PackageTypeID": "PC",
                                    "Weight": isDhl && !isWeihtValidForDhl ?  dhlWeight : cartonData.itemWeightMap[cartons[i]],
                                    "Length": cartonData.shippingBoxMap[cartons[i]].shippingBoxLength,
                                    "Width": cartonData.shippingBoxMap[cartons[i]].shippingBoxWidth,
                                    "Height": cartonData.shippingBoxMap[cartons[i]].shippingBoxHeight,
                                    // "Reference1": "test",
                                    // "Reference2": "test",
                                    "GoodsDescription": cartonData.itemDescriptionMap[cartons[i]]
                            }

                            mappedCartonsData.push(mappedCarton)
                    }
                    log.debug({title:logTitle + " - mapped body data", details:{
                                    mappedCartonsData:mappedCartonsData
                            }})
                    return mappedCartonsData;

            }

            function mapLabelData(exportedCartons,shippingMethod,carrierMapping ){
                    let logTitle = LOG_TITLE + ".mapLabelData -";
                //    log.audit(logTitle);
                    log.debug({
                            title: logTitle + "Input data",
                            details:{
                                    exportedCartons:exportedCartons,
                                    shippingMethod:shippingMethod,
                                    carrierMapping:carrierMapping
                            }
                    })
                    let mappedLabelData = [];
                    let labelOptions = {};


                    labelOptions = {
                        "LabelLanguage": carrierMapping.labelLanguage,
                        "PaperLayout": carrierMapping.LabelLayout
                    };


                    for(let i in exportedCartons)
                    {
                            let labelRequest = {
                                    "ColloMatching": {
                                            "ColloCode": exportedCartons[i].trackingNumber
                                    },
                                    "LabelOptions": labelOptions
                            }

                            mappedLabelData.push(labelRequest)
                    }

                    return mappedLabelData;
            }

            function mapShipmentData(tranData,carrierMapping, isRepushed) {
                    let logTitle = LOG_TITLE + ".mapShipmentData -";
                    log.debug({title:logTitle,details:{tranData:tranData,carrierMapping:carrierMapping}});
                    let waveTrigger = runtime.getCurrentScript().getParameter("custscript_wd_wave_trigger");
                    let shipmentId = waveTrigger ? getShipmentReference(tranData.ifId,tranData.soNumber) : tranData.ifId;
                    let timeStamp = new Date();
                    let bpn;
                    let bpn2;
                    let dc;
                    let xdc;
                    let ds;
                    let fp;
                    let addressMapping = {}

                    bpn = carrierMapping.businessPartner;
                    bpn2 = carrierMapping.businessPartner2;
                    dc = carrierMapping.deliveryCarrier;
                    xdc = carrierMapping.xDeliveryCarrier;
                    if((parseInt(carrierMapping.carrierId) == ONDOT_DHL_CARRIER_ID && tranData.recipientInfo.countryId != "DE") ){
                       ds =  'DHL Paket International Economy'
                    }else if((parseInt(carrierMapping.carrierId) == ONDOT_CHRONO_CARRIER_ID && tranData.recipientInfo.countryId != "FR") ){
                        ds = 'Chrono Classic International toC'
                    }else{
                        ds = carrierMapping.deliveryService;
                    }
                   // ds = (parseInt(carrierMapping.carrierId) == ONDOT_DHL_CARRIER_ID && tranData.recipientInfo.countryId != "DE") ? 'DHL Paket International Economy': carrierMapping.deliveryService; //if the delivery carrier is DHL and it is NOT from DE, it is international shipment
                    //ds = (parseInt(carrierMapping.carrierId) == ONDOT_CHRONO_CARRIER_ID && tranData.recipientInfo.countryId != "FR") ? 'Chrono Classic International toC' : carrierMapping.deliveryService;
                    fp =  carrierMapping.freightPayer;

                log.debug({title:logTitle,details:{tranData:tranData,carrierMapping:carrierMapping}});

                    if( !_isDHLPackStation(tranData.recipientInfo.addressLine) ){
                        addressMapping =   {
                                "Name1": tranData.recipientInfo.recipient,
                                "Name2": tranData.recipientInfo.attention,
                                "AddressLine1": tranData.recipientInfo.addressLine,
                                "AddressLine2":tranData.recipientInfo.addressLine2,
                                "HouseNumber": tranData.recipientInfo.houseNumber,
                                "PostalCode": tranData.recipientInfo.postalCode,
                                "ProvinceCode": tranData.recipientInfo.provinceCode,
                                "City": tranData.recipientInfo.city,
                                "CountryID": tranData.recipientInfo.countryId,
                                "Tel1": tranData.recipientInfo.telephone,
                                "Email": tranData.recipientInfo.email
                        }
                    }else{
                            addressMapping =   {
                                    "Name1": tranData.recipientInfo.recipient,
                                    "Name2": tranData.recipientInfo.attention,
                                    "AddressCategoryID":"PST",
                                    "AddressLine1": "Packstation",
                                    "AddressLine2": tranData.recipientInfo.attention,
                                    "HouseNumber": tranData.recipientInfo.houseNumber,
                                    "PostalCode": tranData.recipientInfo.postalCode,
                                    "City": tranData.recipientInfo.city,
                                    "CountryID": tranData.recipientInfo.countryId,
                                    "Tel1": tranData.recipientInfo.telephone,
                                    "Email": tranData.recipientInfo.email
                            }
                    }

                    let shipmentData = {
                            "Shipment": {
                                    "Number":tranData.soNumber,
                                    "Shipper": {
                                            "BusinessPartnerNumber": bpn
                                    },
                                    "LoadingPoint": {
                                            "BusinessPartnerNumber": bpn2
                                    },
                                    "Recipient": {
                                            "Name": tranData.recipientInfo.recipient,
                                            "Address": addressMapping,
                                    },
                                    "RoutingType": "Delivery",
                                    "DispatchType": "Package",
                                    "DeliveryCarrier": dc,
                                    "xDeliveryCarrier": xdc,
                                    //"DeliveryService": ds,
                                     "ValueOfGoods": tranData.valueOfGoods,
                                    "ValueOfGoodsCurrencyID": tranData.currency,
                                    "GoodsDescription": " Drink more water :) ",
                                    "ShipperReference1":  tranData.soNumber,
                                   // "ShipperReference2":tranData.ifNumber,
                                    "ThirdPartyApplicationList": [
                                            {
                                                    "ReferenceID": shipmentId,
                                                    "UName": "waterdrop.NS"
                                            }
                                    ]
                            },
                            "CreateReturnShipment": false,
                            "RequestedFields": {
                                    "Shipment": {
                                            "Number": true,
                                            "DeliveryCarrierID": true
                                    }
                            }
                    }

                    if(!!fp) { // if the value is not empty then fill in FreightPayer
                            shipmentData["Shipment"]["FreightPayer"] = { "BusinessPartnerNumber": fp }
                    }

                if(!!ds) { // if the value is not empty then fill in Delivery Service
                    shipmentData["Shipment"]["DeliveryService"] = ds
                }
                if(isRepushed){
                    shipmentData["Shipment"]["ShippingDateTimeFrom"] = new Date()
                }

                    if(_isDHLPackStation(tranData.recipientInfo.addressLine)){
                            shipmentData["Shipment"]["AdditonalInformationList"] = [
                                    {
                                            "ThirdPartyID": "PST",
                                            "AdditionalField1":  tranData.recipientInfo.houseNumber,
                                            "AdditionalField2":  tranData.recipientInfo.attention
                                    }
                            ]
                    }

                    if(parseInt(carrierMapping.carrierId) == ONDOT_HERMES_CARRIER_ID){
                            shipmentData["Shipment"]["AdditonalInformationList"] = [
                                    {
                                            "ThirdPartyID": "EMAIL",
                                            "AdditionalField1": tranData.recipientInfo.email,
                                    }
                            ]
                    }
                if(parseInt(carrierMapping.carrierId) == ONDOT_DPD_CARRIER_ID){
                    shipmentData["Shipment"]["AdditonalInformationList"] = [
                        {
                            "ThirdPartyID": "PREDD",
                            "AdditionalField1":  tranData.recipientInfo.email,
                        }
                    ]
                }
                //todo/note: do not uncoomment till comfirmed
                // if(parseInt(carrierMapping.carrierId) == ONDOT_DHL_CARRIER_ID){
                //     shipmentData["Shipment"]["AdditonalInformationList"] = [
                //         {
                //             "ThirdPartyID": "GOGREEN",
                //             "AdditionalField1": "true"
                //         }
                //     ]
                //  }


                    log.debug(logTitle,{"shipmentData":shipmentData,"shipmentDataString": JSON.stringify(shipmentData)});
                    return shipmentData;
            }

        function _isDHLPackStation(address) {
            log.debug("_isDHLPackStation",address)
            return new RegExp(/(PACK|PAKET|POST)\s{0,}(STATION|FACH|FILIALE)/i).test(address.toUpperCase());
        }

            function parseColloResponse(responses,onDotReponseData,repushFlag){
                    let logTitle = LOG_TITLE + ".parseColloResponse -";
                   // log.audit(logTitle);
                    log.audit({
                            title: logTitle + "Input data",
                            details:{
                                    responses:responses,
                                    onDotReponseData:onDotReponseData
                            }
                    });

                    let responseData =  {};
                    let isSuccess = true;
                    let responseMessages = [];
                    let exportedPackages = []

                    for(let i in responses){
                            let response = responses[i];
                            let responseBody = JSON.parse(responses[i].body)

                            if(parseInt(response.code) != 200){

                                    isSuccess = false;
                                    if(responseMessages.indexOf(responseBody.ExceptionMessage) < 0){
                                            responseMessages.push(responseBody.ExceptionMessage);
                                    }
                            }else if(!isEmpty(responseBody)){

                                    let exportedPackage = {};
                                    if(!repushFlag){
                                            exportedPackage.number = responseBody.Collo.ColloCode //todo responseBody.Collo.Number;
                                            exportedPackage.trackingNumber = responseBody.Collo.TrackingNumber;
                                            exportedPackages.push(exportedPackage)
                                    }else{
                                            for(let j in responseBody.ColloList){
                                                    exportedPackage = {};
                                                    exportedPackage.number = responseBody.ColloList[j].ColloCode //todo responseBody.ColloList[j].Number;
                                                    exportedPackage.trackingNumber = responseBody.ColloList[j].TrackingNumber;
                                                    exportedPackages.push(exportedPackage)
                                            }
                                    }

                            }
                    }
                    responseData.status = isSuccess ? SUCCESS_STATUS_ID : ERROR_STATUS_ID;
                    responseData.message = responseMessages.join(";")
                    onDotReponseData.status = isSuccess ? onDotReponseData.status : ERROR_STATUS_ID;
                    onDotReponseData.message = isSuccess ? onDotReponseData.message :  responseMessages.join(";")
                    responseData.exportedPackages = exportedPackages;
                    log.debug(logTitle + " - responseData", responseData);
                    return responseData
            }


            //set and save functions
            function setPackagesOnIf(itemFulfillmentRec,trackingNumbersData,itemWeightMap){
                    let logTitle = LOG_TITLE + ".setPackagesOnIf -";
                  //  log.audit(logTitle);
                    log.audit({
                            title: logTitle + "Input data",
                            details:{
                                    trackingNumbersData:trackingNumbersData,
                                    itemWeightMap:itemWeightMap
                            }
                    });
                    //removing empty line
                    let lineCount = itemFulfillmentRec.getLineCount("package");
                    if(lineCount > 0){
                        for(let k = 0; k < lineCount;k++){
                            let trackLink = itemFulfillmentRec.getSublistValue({sublistId: 'package', fieldId: "packagedescr",line:k})
                            let trackNumber = itemFulfillmentRec.getSublistValue({sublistId: 'package', fieldId: "packagetrackingnumber",line:k})
                            if(isEmpty(trackLink) && isEmpty(trackNumber)){
                                itemFulfillmentRec.removeLine({sublistId:"package",line:k})

                            }
                        }

                    }
                    for(let i in trackingNumbersData){
                            let data = trackingNumbersData[i];
                            let lineWitTrackingNum =  itemFulfillmentRec.findSublistLineWithValue({
                                    sublistId: 'package',
                                    fieldId: 'packagetrackingnumber',
                                    value: data.TrackingNumber
                            });
                            if(!isEmpty(data.TrackingLink) && lineWitTrackingNum == -1){
                                    itemFulfillmentRec.selectNewLine({sublistId:'package'});
                                    itemFulfillmentRec.setCurrentSublistValue({sublistId: 'package', fieldId: 'packagetrackingnumber', value: data.TrackingNumber});
                                    itemFulfillmentRec.setCurrentSublistValue({sublistId: 'package', fieldId: 'packagedescr', value: data.TrackingLink});
                                    itemFulfillmentRec.setCurrentSublistValue({sublistId: 'package', fieldId: 'packageweight', value: isEmpty(data.labelData.weight) ? 1 : data.labelData.weight});
                                    itemFulfillmentRec.commitLine({sublistId:'package'});
                                    log.debug(logTitle, "The packages on IF set");
                            }else{
                                    log.debug(logTitle, "The packages on IF already set/tracklink is missing, new line is NOT added!");
                            }

                    }

            }

            function saveLabelsOnIf(itemFulfillmentRec, labelsData,trackingNumbersData){
                    let logTitle = LOG_TITLE + ".saveLabelsOnIf -";
                    log.audit({title:logTitle + " - Input data", details:{ trackingNumbersData:trackingNumbersData}});

                    let counter = 0;

                    for(let i in trackingNumbersData)  {
                            let data = trackingNumbersData[i];
                            let codeListCodes =  data.CodeList.map(codeObj => codeObj.Code);
                            itemFulfillmentRec.setCurrentSublistValue({sublistId: 'recmachcustrecord_wd_item_fulfillment', fieldId: 'custrecord_wd_tracking_number', value: data.TrackingNumber});
                            itemFulfillmentRec.setCurrentSublistValue({sublistId: 'recmachcustrecord_wd_item_fulfillment', fieldId: 'custrecord_wd_track_link', value: data.TrackingLink});
                            itemFulfillmentRec.setCurrentSublistValue({sublistId: 'recmachcustrecord_wd_item_fulfillment', fieldId: 'custrecord_wd_carrier_label', value: data.labelData.url  });
                            itemFulfillmentRec.setCurrentSublistValue({sublistId: 'recmachcustrecord_wd_item_fulfillment', fieldId: 'custrecord_wd_weight', value: data.labelData.weight});
                            itemFulfillmentRec.setCurrentSublistValue({sublistId: 'recmachcustrecord_wd_item_fulfillment', fieldId: 'custrecord_wd_carton_id', value: data.labelData.number});
                            itemFulfillmentRec.setCurrentSublistValue({sublistId: 'recmachcustrecord_wd_item_fulfillment', fieldId: 'custrecord_wd_related_barcodes', value: (codeListCodes).join(";")});
                            itemFulfillmentRec.commitLine({sublistId:'recmachcustrecord_wd_item_fulfillment'});
                            counter++;
                          //   let valueFieldMap = {
                          //       "custrecord_wd_tracking_number":data.TrackingNumber,
                          //       "custrecord_wd_track_link":data.TrackingLink,
                          //       "custrecord_wd_carrier_label":data.labelData.url,
                          //       "custrecord_wd_weight":data.labelData.weight,
                          //       "custrecord_wd_carton_id":data.labelData.number,
                          //       "custrecord_wd_related_barcodes":(codeListCodes).join(";"),
                          //       "custrecord_wd_item_fulfillment":itemFulfillmentRec.getValue('id')
                          //   }
                          // let lastLabelToSet =   createRecord('customrecord_wd_labels_ondot',valueFieldMap)
                          //   itemFulfillmentRec.setValue({fieldId:"custbody_wd_last_label" , value:lastLabelToSet})

                    }

            }

            //[LZ note]--> here we are returning the data from ondot (url) and adding weight from weight map
            function saveLabelFiles(labelRequestData,urlLabelRequest,headers, itemWeightMap,onDotReponseData){
                    let logTitle = LOG_TITLE + ".saveLabelFiles -";
                    let script  = runtime.getCurrentScript()
                    let folderLabelsId= script.getParameter({name:"custscript_wdff_folder_for_labels"})
                    let retLinks = {};
                    let printLabels = true;
                    log.debug(logTitle, {urlLabelRequest:urlLabelRequest,itemWeightMap:itemWeightMap, labelRequestData:JSON.stringify(labelRequestData)});
                    if(labelRequestData.length > 30){
                        printLabels = false
                    }
                    log.debug(logTitle, {printLabels:printLabels,labelRequestDataLENGTH: labelRequestData.length});

                    labelRequestData.forEach((bodyData) => {

                            labelResponse = https.post({
                                    url: urlLabelRequest,
                                    body: JSON.stringify(bodyData),//JSON.stringify(getTestData(tranData.soNumber, 888));
                                    headers: headers
                            });
                            //code 503 == service unavailable
                            log.audit(logTitle, "labelResponse: " + JSON.stringify(labelResponse));

                            let body = JSON.parse(labelResponse.body);
                            let code = labelResponse.code;
                            let labelType = bodyData.LabelOptions.LabelLanguage.toUpperCase();
                            if(body && code == 200)
                            {
                                    // body = JSON.parse();
                                    let labels = body.Labels;

                                    if(labels && labels.length > 0 && printLabels)
                                    {
                                            let base64file = labels[0];
                                            let fileType;
                                            let ext;

                                            if(labelType == 'PDF')
                                            {
                                                    fileType = file.Type.PDF;
                                                    ext = 'PDF'
                                            }
                                            else if(labelType == 'JPG' || labelType == 'JPEG')
                                            {
                                                    fileType = file.Type.JPGIMAGE;
                                                    ext = 'jpg';
                                            }

                                            let newFile = file.create({
                                                    name: bodyData.ColloMatching.ColloCode +'.'+ext,  //todo: bodyData.ColloMatching.Number+'.'+ext,
                                                    fileType : fileType,
                                                    contents : base64file,
                                                    folder : folderLabelsId
                                            });

                                            let fileId = newFile.save();
                                            let fileUrl = search.lookupFields({
                                                    type: 'file',
                                                    id: fileId,
                                                    columns: ['url']
                                            });

                                            if(fileUrl)
                                            {
                                                    fileUrl = fileUrl.url;
                                            }


                                        retLinks[bodyData.ColloMatching.ColloCode] = {number: bodyData.ColloMatching.ColloCode, url: fileUrl, weight:itemWeightMap[bodyData.ColloMatching.ColloCode] };

                                    }else if(!printLabels){ //limit exceeded!

                                        retLinks[bodyData.ColloMatching.ColloCode] = {number: bodyData.ColloMatching.ColloCode, url: "", weight:itemWeightMap[bodyData.ColloMatching.ColloCode] };
                                    }
                            }else{
                                    onDotReponseData.status = PARTIAL_SUCCESS_ID;
                                    onDotReponseData.message += body.ExceptionMessage + ";"
                                 //   log.debug(logTitle, "onDotReponseData - "+ JSON.stringify(onDotReponseData))
                            }

                    });
                    log.audit(logTitle, "retLinks: "+JSON.stringify(retLinks));

                    return retLinks;
            }

            function mergeLabelsAndTrackingNumbers(labelsData,trackingNumbersData,itemWeightMap){
                    let logTitle = LOG_TITLE + ".mergeLabelsAndTrackingNumbers -";
                    for(let i in trackingNumbersData)  {
                            trackingNumbersData[i].labelData = getAssociatedLabelData(trackingNumbersData[i].CodeList,labelsData,itemWeightMap)
                    }
                    return trackingNumbersData;
            }



            function isEmpty(data) {
                    return ( data == '' || data == null || (typeof data == 'undefined') || (data.constructor === Object && Object.keys(data).length === 0))
            }



            function getShipmentReference(itemFulfillmentId,soNumber)
            {
                    let logTitle = LOG_TITLE + ".getShipment -";
                  //  log.audit(logTitle);
                    log.debug({
                            title: logTitle + "Input data",
                            details:{
                                    itemFulfillmentId:itemFulfillmentId,
                                    soNumber:soNumber
                            }
                    })
                    let waveNumber = '';
                    let shipmentReference = '';
                    let waveSearch = search.create({
                            type: "wave",
                            filters:
                                [
                                        ["type","anyof","Wave"],
                                        "AND",
                                        ["status","anyof","Wave:D"],
                                        "AND",
                                        ["applyingtransaction","anyof",itemFulfillmentId]
                                ],
                            columns:
                                [
                                        "tranid",
                                        "internalid",
                                        "applyingtransaction",
                                        "appliedtotransaction"
                                ]
                    });

                    let searchResultCount = waveSearch.runPaged().count;

                    if(searchResultCount == 0){ //if the itemfulfillment does not have any related Wave, the shipment does not exist in shippingNET -  it was never before pushed to shippingNET
                            throw "The item fulfillment has no related Wave, shippingNET integration is terminated!"
                    }

                    waveSearch.run().each(function(result){
                            waveNumber = result.getValue({name: "tranid"})
                            return true;
                    });

                    shipmentReference = waveNumber +"_"+soNumber;
                    return shipmentReference;
            }

        function createRecord(recordType, valuesToSet){
            let LOG_TITLE = "<< createRecord >>";
            log.debug(LOG_TITLE + " - Input data -", {valuesToSet:valuesToSet, recordType: recordType});

            let customRec = record.create({type:recordType});

            for(let fieldId in valuesToSet){
                let fieldValue = valuesToSet[fieldId]
                customRec.setValue(fieldId,fieldValue )
            }

            let recordId = customRec.save();
            log.debug(LOG_TITLE,"Record created- " + recordId);

            return recordId;

        }

        function round2DecPlaces(num) {
            return Math.round((num + Number.EPSILON) * 100) / 100;
        }

    });
