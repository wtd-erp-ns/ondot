/**
 * Module Description:
 *
 * Version       Date              Author                Remarks
 * 1.0           30 Nov 2020       Martin Strasil        Initial Version
 *
 * @NApiVersion 2.1
 * @NModuleScope Public
 */
define(['require', 'exports', 'N/runtime', 'N/search', "./../lib/ilog"], function(require, exports) {
    const runtime = require('N/runtime');
    const search = require('N/search');
    const ilog = require("./../lib/ilog");

    const LOG_TITLE = ilog.LOG_TITLE+'Config.';

    /**
     * Extract details from configuration record into an object.
     *
     * @param {number} configId - internal id of configuration record
     * @param {boolean} useProdUrl - whenever use dev or prod url
     * @returns {object}
     * @returns {object.iaListUrl} - item-movement-status list URL
     */
    exports.getConfig = (configId, useProdUrl, overrideEnvironmentSetting) => {
        let objConfig = null;
        const logTitle = LOG_TITLE+'getConfig';

        try {
            let configRecordValues = search.lookupFields({
                type: configRecord.id,
                id: configId,
                columns: configRecord.getAllFieldsInArray()
            });

            let environment = runtime.envType;
            let baseUrl;
            let baseTokenUrl;
            let envType;

            // If we need to override environment and query SB endpoint from PRODUCTION and/or vice versa
            if(overrideEnvironmentSetting)
            {
                baseUrl = useProdUrl ? configRecordValues[configRecord.fields.URL] : configRecordValues[configRecord.fields.SB_URL];
                envType = useProdUrl ? runtime.EnvType.PRODUCTION : runtime.EnvType.SANDBOX;

                // Used dedicated token URL if provided, otherwise use base URL
                if(useProdUrl)
                {
                    baseTokenUrl = configRecordValues[configRecord.fields.AUTH_URL] ? configRecordValues[configRecord.fields.AUTH_URL] : baseUrl;
                }
                else
                {
                    baseTokenUrl = configRecordValues[configRecord.fields.AUTH_URL_SB] ? configRecordValues[configRecord.fields.AUTH_URL_SB] : baseUrl;
                }
            }
            else
            {
                envType = environment;
                if(environment == runtime.EnvType.PRODUCTION)
                {
                    baseUrl = configRecordValues[configRecord.fields.URL];
                    baseTokenUrl = configRecordValues[configRecord.fields.AUTH_URL] ? configRecordValues[configRecord.fields.AUTH_URL] : baseUrl;
                }
                else
                {
                    baseUrl = configRecordValues[configRecord.fields.SB_URL];
                    baseTokenUrl = configRecordValues[configRecord.fields.AUTH_URL_SB] ? configRecordValues[configRecord.fields.AUTH_URL_SB] : baseUrl;
                }
            }

            log.debug(logTitle, JSON.stringify(configRecordValues));
            log.debug(logTitle, 'configRecord.fields.TOKEN: '+configRecord.fields.TOKEN);
			objConfig = {
                name: configRecordValues[configRecord.fields.NAME],
                baseUrl: baseUrl,
                tokenUrl: baseTokenUrl + configRecordValues[configRecord.fields.TOKEN],
                user: configRecordValues[configRecord.fields.USER],
				passwordGuid: configRecordValues[configRecord.fields.PSWD_GUID],
				iaListUrl: baseUrl + configRecordValues[configRecord.fields.IA_LIST],
				iaGetUrl: baseUrl + configRecordValues[configRecord.fields.IA_GET],
                iaCommitUrl: baseUrl + configRecordValues[configRecord.fields.IA_COMMIT],
                itemMaster : baseUrl + configRecordValues[configRecord.fields.ITEM_MASTER],
                clientId: configRecordValues[configRecord.fields.CLIENT_ID],
                clientSecret: configRecordValues[configRecord.fields.CLIENT_SECRET],
                orgUnitId: configRecordValues[configRecord.fields.ORG_UNIT_ID],
                importOrder: configRecordValues[configRecord.fields.IMPORT_ORDER],
				exportBoxes: configRecordValues[configRecord.fields.EXPORT_BOXES],
				requestLabel: configRecordValues[configRecord.fields.REQUEST_LABEL],
				dailyClosing: configRecordValues[configRecord.fields.DAILY_CLOSING],
                receiptAviso: baseUrl +  configRecordValues[configRecord.fields.RECEIPT_AVISOS],
                dispatchOrder: baseUrl +  configRecordValues[configRecord.fields.DISPATCH_ORDER],
                dispatchOrderConfirmation: baseUrl +  configRecordValues[configRecord.fields.DISPATCH_ORDER_CONFIRMATION],
                receiptConfirmation: baseUrl +  configRecordValues[configRecord.fields.RECEIPT_CONFIRMATION],
                envType : envType
			 };

        } catch (e) {
            // TODO MS: log and handle error
            log.error(logTitle,e.name+': '+e.message);

        } finally {

            return objConfig;
        }
	};

    /**
     * Object of configuration record definition.
     */
    const configRecord = {
        id: 'customrecord_wd_3pl_integration_config',
        fields: {
			ID: 'internalid',
			NAME: 'name',
            USER: 'custrecord_wd_3pl_ic_user',
			PSWD_GUID: 'custrecord_wd_3pl_ic_pswd_guid',
			URL: 'custrecord_wd_3pl_ic_base_url',
            SB_URL: 'custrecord_wd_3pl_ic_sb_base_url',
            AUTH_URL: 'custrecord_wd_3pl_ic_auth_url',
			AUTH_URL_SB: 'custrecord_wd_3pl_ic_auth_url_sb',
            TOKEN: 'custrecord_wd_3pl_ic_token_endpoint',
            TOKEN_IFACE: 'custrecord_wd_3pl_ic_token_iface',
			PO_LIST: 'custrecord_wd_3pl_ic_po_list',
			PO_GET: 'custrecord_wd_3pl_ic_po_get',
			PO_COMMIT: 'custrecord_wd_3pl_ic_po_commit',
			IA_LIST: 'custrecord_wd_3pl_ic_ia_list',
			IA_GET: 'custrecord_wd_3pl_ic_ia_get',
            IA_COMMIT: 'custrecord_wd_3pl_ic_ia_commit',
            ITEM_MASTER: 'custrecord_wd_3pl_ic_item_master',
            CLIENT_ID: 'custrecord_wd_3pl_ic_client_id',
            CLIENT_SECRET: 'custrecord_wd_3pl_client_secret',
            ORG_UNIT_ID: 'custrecord_wd_3pl_ic_org_unit_id',
            IMPORT_ORDER: 'custrecord_wd_3pl_ic_import_order',
			EXPORT_BOXES: 'custrecord_wd_3pl_ic_export_boxes',
			REQUEST_LABEL :'custrecord_wd_3pl_ic_request_label',
			DAILY_CLOSING : 'custrecord_wd_3pl_ic_daily_closing',
            RECEIPT_AVISOS: 'custrecord_wd_3pl_ic_receipt_avis',
            DISPATCH_ORDER: 'custrecord_wd_3pl_dispatch_order',
            DISPATCH_ORDER_CONFIRMATION: 'custrecord_wd_3pl_dispatch_order_confirm',
            RECEIPT_CONFIRMATION: "custrecord_wd_3pl_dispatch_order_confirm"
        },
        getAllFieldsInArray() {
            return Object.values(this.fields);
        }
    };
});