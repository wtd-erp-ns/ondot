/**
 * Module Description:
 *
 * Version       Date              Author                Remarks
 * 1.0           07 Dec 2020       Libor Nekula        Initial Version
 *
 * @NApiVersion 2.1
 * @NModuleScope Public
 *
 */

define(['require', 'exports', 'N/error', 'N/record'], function(require, exports) {
    const error = require('N/error');
    const record = require('N/record');

    exports.LOG_TITLE = 'WD_3PL.'
    exports.LOG_STATE = {
        NEW: '1',
        SUCCESS: '2',
        ERROR: '3'
    }

    exports.INTERFACES = {
        MHD1 : 2,
        MHD2 : 3,
        MHD3 : 4,
        CP00 : 7,
        CP01 : 9,
      	SN01 : 10 
    }

	/**
     * Get script parameters
     *
     * @param {Array} parameterList - Array defining expected script parameters
     * @returns {object} params - contains map of parameters and their values
     */
    exports.logIntegration = (options) => {

        const MAX_STRING = 100000;
        let logRec;

        if(options.id)
        {
            logRec = record.load({
                type : 'customrecord_av_integration_log',
                id : options.id
            });
        }
        else
        {
            logRec = record.create({
                type : 'customrecord_av_integration_log'
            });
        }

        if(options.request)
        {
            logRec.setValue({
                fieldId: 'custrecord_av_il_send',
                value: options.request
            })
        }

        if(options.response)
        {
            logRec.setValue({
                fieldId: 'custrecord_av_il_receive',
                value: options.response
            });
        }

        if(options.interface)
        {
            logRec.setValue({
                fieldId: 'custrecord_av_il_type',
                value: options.interface
            });
        }

        if(options.transaction)
        {
            logRec.setValue({
                fieldId: 'custrecord_av_il_transaction',
                value: options.transaction
            });
        }

        if(options.entity)
        {
            logRec.setValue({
                fieldId: 'custrecord_av_il_entity',
                value: options.transaction
            });
        }

        if(options.item)
        {
            logRec.setValue({
                fieldId: 'custrecord_av_il_item',
                value: options.item
            });
        }

        if(options.state)
        {
            logRec.setValue({
                fieldId: 'custrecord_av_il_state',
                value: options.state
            });
        }

        if(options.url)
        {
            logRec.setValue({
                fieldId: 'custrecord_av_il_url',
                value: options.url
            });
        }

        if(options.code)
        {
            logRec.setValue({
                fieldId: 'custrecord_av_il_code',
                value: options.code
            });
        }

        if(options.staging)
        {
            logRec.setValue({
                fieldId: 'custrecord_av_il_3pl_staging',
                value: options.staging
            });
        }

        let logRecId = logRec.save();

        return logRecId;
    };

    /**
     * Updates status on integration log record
     *
     * @param {String} logId
     * @param {String} status
     * @return {Void} stagingRecId - Internal Id of newly created staging record
     */
    exports.updateLogStatus = (logId, status) => {

        const logTitle = 'updateLogStatus';
        log.debug(logTitle, '>> Entry <<');

        let updateValues =
        {
            custrecord_av_il_state : status
        };

        log.debug(logTitle, JSON.stringify(updateValues));

        record.submitFields({
            type: 'customrecord_av_integration_log',
            id: logId,
            values: updateValues
        });

        log.debug(logTitle, '>> Exit <<');
    }
});